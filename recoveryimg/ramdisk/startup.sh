# check_pstate.sh

/system/bin/busybox --install /system/bin

# PSTATE=$(( $(cat /sys/pmgr/pstate) & 0x0A ))

# case $(( $PSTATE & 0x0A )) in
# 10)
#     echo "Booting with USB connected, don't start charger"
#     ;;

# 8)
#     echo "Booting normal mode, starting recovery"
#     # setprop sys.recovery.start 1
#     /sbin/recovery
#     ;;
# esac

sleep 3

USB=$(cat /sys/class/android_usb/android0/state)

case $USB in
"CONNECTED")
    echo "Booting with USB connected, don't start charger"
    echo "Booting with USB connected, don't start charger" > /dev/kmsg
    break
    ;;
"CONFIGURED")
    echo "Booting with USB connected, don't start charger"
    echo "Booting with USB connected, don't start charger" > /dev/kmsg
    ;;
*)
    echo "Booting normal mode, starting recovery"
    echo "Booting normal mode, starting recovery" > /dev/kmsg
    # setprop sys.recovery.start 1
    /sbin/recovery
    ;;
esac
