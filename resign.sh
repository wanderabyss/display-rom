SYS_CERT_APKS="\
./system/app/CertInstaller/CertInstaller.apk
./system/app/Provision/Provision.apk
./system/app/KeyChain/KeyChain.apk
./system/app/PackageInstaller/PackageInstaller.apk
./system/app/Bluetooth/Bluetooth.apk
./system/priv-app/ExternalStorageProvider/ExternalStorageProvider.apk
./system/priv-app/Obd/Obd.apk
./system/priv-app/DefaultContainerService/DefaultContainerService.apk
./system/priv-app/SettingsProvider/SettingsProvider.apk
./system/priv-app/InputDevices/InputDevices.apk
./system/priv-app/ANCS/ANCS.apk
./system/priv-app/Settings/Settings.apk
./system/priv-app/Hud/Hud.apk
./system/priv-app/Shell/Shell.apk
./system/priv-app/FusedLocation/FusedLocation.apk
./system/framework/framework-res.apk
"

for APK in $SYS_CERT_APKS
do
apksigner sign --ks navdy_cert.jks --ks-pass=env:JP --out $APK $APK
done


