#/bin/sh

#remove anything that may have existed
iptables -t nat -D OUTPUT -p tcp -j REDSOCKS
iptables -t nat -D OUTPUT -d 127.0.0.0/8 -p udp -m udp --dport 53 -j RETURN
iptables -t nat -D OUTPUT -p udp -m udp --dport 53 -j DNAT --to-destination 127.0.0.1:53
iptables -t nat -F REDSOCKS

# set up our tables
iptables -t nat -N REDSOCKS
iptables -t nat -A OUTPUT -p tcp -j REDSOCKS
iptables -t nat -A OUTPUT -d 127.0.0.0/8 -p udp -m udp --dport 53 -j RETURN
iptables -t nat -A OUTPUT -p udp -m udp --dport 53 -j DNAT --to-destination 127.0.0.1:53
iptables -t nat -A REDSOCKS -d 0.0.0.0/8 -j RETURN
iptables -t nat -A REDSOCKS -d 10.0.0.0/8 -j RETURN
iptables -t nat -A REDSOCKS -d 127.0.0.0/8 -j RETURN
iptables -t nat -A REDSOCKS -d 169.254.0.0/16 -j RETURN
iptables -t nat -A REDSOCKS -d 172.16.0.0/12 -j RETURN
iptables -t nat -A REDSOCKS -d 192.168.0.0/16 -j RETURN
iptables -t nat -A REDSOCKS -d 224.0.0.0/4 -j RETURN
iptables -t nat -A REDSOCKS -d 240.0.0.0/4 -j RETURN
iptables -t nat -A REDSOCKS -p tcp -j REDIRECT --to-ports 12345
